# Warning when using transitiveExport

This sample project demonstrate an issue when we export libraries to iOS using the `transitiveExport = true` option in KMM. See [build.gradle.kts](build.gradle.kts).

I asked on [StackOverflow](https://stackoverflow.com/questions/77475422/xxxkotlincharcompanion-could-not-be-mapped-to-kotlinchar-companion).

package fr.ryfacto.exportissue

import kotlinx.datetime.LocalDate
import kotlinx.datetime.Month

class Greeting {
    private val platform: Platform = getPlatform()
    
    val localDate = LocalDate(year = 2023, month = Month.APRIL, dayOfMonth = 22)

    fun greet(): String {
        return "Hello, ${platform.name}!"
    }
}

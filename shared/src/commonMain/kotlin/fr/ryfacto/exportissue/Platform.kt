package fr.ryfacto.exportissue

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform
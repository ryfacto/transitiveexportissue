import SwiftUI
import shared

struct ContentView: View {
	let greet = Greeting().greet()
	let date = Greeting().localDate

	var body: some View {
        VStack {
            Text(greet)
            Spacer()
            Text(date.description())
        }
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
	}
}
